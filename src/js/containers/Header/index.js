import React, { Component } from 'react';
import cx from 'classnames';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Button } from '@material-ui/core';

import { loadProfile } from 'actions';
import Fetching from 'components/Fetching';

import css from 'styles/containers/Header.scss';

class HeaderContainer extends Component {
  componentDidMount() {
    this.loadProfile();
  }

  loadProfile = async () => {
    const { loadProfileAction } = this.props;
    await loadProfileAction();
  };

  getUserProfile = () => {
    const {
      user: { data, loading, loaded },
    } = this.props;
    const userData = loaded && data.data.data;
    return { userData, loaded, loading };
  };

  render() {
    const { logout } = this.props;
    const { userData, loaded, loading } = this.getUserProfile();

    return (
      <header className={cx(css.header, css.header_global)}>
        <div className={css.header__container}>
          <div style={{ width: 'auto' }}>
            <Fetching isFetching={loading} size={20} thickness={5}>
              <p>{loaded && userData.email}</p>
            </Fetching>
          </div>

          <Button variant="contained" color="primary" size="small" onClick={logout}>
            Logout
          </Button>
        </div>
      </header>
    );
  }
}

export default connect(
  (state) => ({
    user: state.get.user,
  }),
  (dispatch) => ({
    loadProfileAction: () => dispatch(loadProfile()),
  }),
)(withRouter(HeaderContainer));
