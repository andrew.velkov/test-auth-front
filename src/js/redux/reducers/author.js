const initialState = {
  loaded: false,
  loading: false,
  data: [],
};

export default function author(param = '') {
  return (state = initialState, action = {}) => {
    switch (action.type) {
      case `author/${param}/REQUEST`:
        return {
          ...state,
          loading: true,
          loaded: false,
        };
      case `author/${param}/SUCCESS`:
        return {
          ...state,
          loading: false,
          loaded: true,
          data: action.payload,
        };
      case `author/${param}/FAILURE`:
        return {
          ...state,
          loading: false,
          loaded: false,
        };
      default:
        return state;
    }
  };
}
