import { combineReducers } from 'redux';

// api
import user from './user';
import author from './author';

// other
import authentication from './authentication';
import notification from './notification';

const reducers = combineReducers({
  get: combineReducers({
    authentication: authentication('authentication'),
    user: user('profile'),
    getAuthors: author('getAuthors'),
    notifications: notification('notification'),
  }),
  post: combineReducers({
    sendRegister: user('register'),
    sendLogin: user('login'),
  }),
});

export default reducers;
