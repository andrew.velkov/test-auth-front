import * as type from 'constants/author';

export const getAuthors = () => ({
  types: [type.GET_AUTHORS_REQUEST, type.GET_AUTHORS_SUCCESS, type.GET_AUTHORS_FAILURE],
  payload: {
    request: {
      url: '/author',
      method: 'GET',
    },
  },
});

export const addAuthor = (data) => ({
  types: [type.ADD_AUTHOR_REQUEST, type.ADD_AUTHOR_SUCCESS, type.ADD_AUTHOR_FAILURE],
  payload: {
    request: {
      url: '/author',
      method: 'POST',
      data,
    },
  },
});

export const updateAuthor = (params, data) => ({
  types: [type.UPDATE_AUTHOR_REQUEST, type.UPDATE_AUTHOR_SUCCESS, type.UPDATE_AUTHOR_FAILURE],
  payload: {
    request: {
      url: `/author/${params.authorId}`,
      method: 'PUT',
      data,
    },
  },
});

export const removeAuthor = (params) => ({
  types: [type.REMOVE_AUTHOR_REQUEST, type.REMOVE_AUTHOR_SUCCESS, type.REMOVE_AUTHOR_FAILURE],
  payload: {
    request: {
      url: `/author/${params.authorId}`,
      method: 'DELETE',
    },
  },
});
