import { register, login, isLoggedIn, loadProfile, logout } from './user';

import { getAuthors, addAuthor, updateAuthor, removeAuthor } from './author';

import { addNotification, removeNotification } from './other';

export {
  // users
  register,
  login,
  isLoggedIn,
  loadProfile,
  logout,
  // authors
  getAuthors,
  addAuthor,
  updateAuthor,
  removeAuthor,
  // other
  addNotification,
  removeNotification,
};
