import React, { Component } from 'react';
import { Icon, Popover, Paper, Button, Grid, IconButton } from '@material-ui/core';

import strings from 'translations';

class Buttons extends Component {
  state = {
    isOpenPopover: false,
    anchorEl: null,
  };

  handleOpenPopover = (e) => {
    this.setState({
      isOpenPopover: true,
      anchorEl: e.currentTarget,
    });
  };

  handleClosePopover = () => {
    this.setState({
      isOpenPopover: false,
    });
  };

  render() {
    const {
      types = '',
      iconName,
      handleRemove,
      buttonRemoveColor = 'secondary',
      buttonRemoveName,
      buttonRemoveDisabled,
    } = this.props;
    const { isOpenPopover, anchorEl } = this.state;

    return (
      <React.Fragment>
        {types === 'remove' && (
          <React.Fragment>
            {!iconName ? (
              <Button
                size="small"
                variant="contained"
                color={buttonRemoveColor}
                disabled={buttonRemoveDisabled}
                onClick={this.handleOpenPopover}
              >
                {buttonRemoveName}
              </Button>
            ) : (
              <IconButton
                color={buttonRemoveColor}
                disabled={buttonRemoveDisabled}
                onClick={this.handleOpenPopover}
              >
                <Icon>{iconName}</Icon>
              </IconButton>
            )}
            <Popover
              open={isOpenPopover}
              anchorEl={anchorEl}
              onClose={this.handleClosePopover}
              anchorOrigin={{
                vertical: 'center',
                horizontal: 'center',
              }}
              transformOrigin={{
                vertical: 'center',
                horizontal: 'center',
              }}
            >
              <Paper style={{ padding: 15, width: '100%' }}>
                <h4>{strings.other.confirm_action}</h4>
                <Grid container justify="space-between" spacing={4}>
                  <Grid item>
                    <Button
                      variant="contained"
                      color={buttonRemoveColor}
                      size="small"
                      onClick={handleRemove}
                    >
                      {buttonRemoveName}
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button color="primary" size="small" onClick={this.handleClosePopover}>
                      {strings.buttons.close}
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
            </Popover>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default Buttons;
