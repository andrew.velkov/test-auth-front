import React from 'react';
import { Dialog as DialogMaterial, DialogContent } from '@material-ui/core';

const Dialog = ({ isOpenDialog, handleCloseDialog, children }) => (
  <DialogMaterial open={isOpenDialog} onClose={handleCloseDialog}>
    <DialogContent>{children}</DialogContent>
  </DialogMaterial>
);

export default Dialog;
