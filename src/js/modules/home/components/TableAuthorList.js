import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton,
  Icon,
} from '@material-ui/core';

import { HEAD_AUTHOR } from 'config';
import ButtonRemomve from 'components/Button';

import css from 'styles/pages/Home.scss';

const TableAuthorList = ({ authorList, handleOpenDialog, handleRemoveAuthor }) => (
  <div className={css.tableWrap}>
    <TableContainer className={css.tableWrap__container} component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            {HEAD_AUTHOR.map((item) => (
              <TableCell
                key={item}
                className={css.tableWrap__th}
                align={item === 'Actions' ? 'right' : ''}
              >
                {item}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody className={css.TableBody}>
          {authorList.map((author, index) => (
            <TableRow key={author._id}>
              <TableCell>{index + 1}</TableCell>
              <TableCell>{author.first_name}</TableCell>
              <TableCell>{author.last_name}</TableCell>
              <TableCell>
                <div className={css.tableWrap__comments}>{author.comments}</div>
              </TableCell>
              <TableCell align="right">
                <div className={css.tableWrap__actions}>
                  <IconButton color="primary" size="small" onClick={handleOpenDialog(author)}>
                    <Icon>edit</Icon>
                  </IconButton>
                  <ButtonRemomve
                    types="remove"
                    iconName="delete"
                    buttonRemoveName="Yes, delete!"
                    handleRemove={handleRemoveAuthor(author._id)}
                  />
                </div>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  </div>
);

export default TableAuthorList;
