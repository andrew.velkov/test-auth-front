import React from 'react';
import cx from 'classnames';
import { Grid, Button } from '@material-ui/core';

import Input from 'components/Form/Input';
import Formik from 'helpers/Formik';
import { AuthorSchema } from 'helpers/Formik/validation';

import css from 'styles/components/Dialog.scss';

const FormAuthorDialog = ({ handleCloseDialog, handleSubmitDialog, data }) => (
  <Formik
    initialValues={{
      first_name: data ? data.first_name : '',
      last_name: data ? data.last_name : '',
      comments: data ? data.comments : '',
    }}
    validationSchema={AuthorSchema}
    onSubmit={handleSubmitDialog}
  >
    {({ values, errors, touched, handleChange, handleSubmit, isSubmitting }) => (
      <form>
        <h3>{data ? 'Update' : 'Add'}</h3>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <Input
              label="First Name"
              variant="standard"
              name="first_name"
              error={errors.first_name && touched.first_name}
              errorText={errors.first_name && touched.first_name && errors.first_name}
              value={values.first_name}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <Input
              label="Last Name"
              variant="standard"
              name="last_name"
              error={errors.last_name && touched.last_name}
              errorText={errors.last_name && touched.last_name && errors.last_name}
              value={values.last_name}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12}>
            <Input
              label="Comments"
              variant="standard"
              name="comments"
              multiline
              rows={2}
              rowsMax={5}
              error={errors.comments && touched.comments}
              errorText={errors.comments && touched.comments && errors.comments}
              value={values.comments}
              onChange={handleChange}
            />
          </Grid>
        </Grid>

        <ul className={cx(css.buttonGroup, css.buttonGroup_offset)}>
          <li className={css.buttonGroup__item}>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              disabled={isSubmitting}
              onClick={handleSubmit}
            >
              {data ? 'Update' : 'Save'}
            </Button>
          </li>
          <li className={css.buttonGroup__item}>
            <Button onClick={handleCloseDialog} className={cx(css.button__top)}>
              Close
            </Button>
          </li>
        </ul>
      </form>
    )}
  </Formik>
);

export default FormAuthorDialog;
