import React from 'react';
import cx from 'classnames';
import { connect } from 'react-redux';
import { Button } from '@material-ui/core';

import { getAuthors, addAuthor, updateAuthor, removeAuthor } from 'actions';
import Dialog from 'components/Dialog';
import Fetching from 'components/Fetching';
import Well from 'components/Well';

import css from 'styles/pages/Home.scss';

import TableAuthorList from '../components/TableAuthorList';
import FormAuthorDialog from '../components/FormAuthorDialog';

class HomeContainer extends React.Component {
  state = {
    isOpenDialog: false,
    data: null,
  };

  componentDidMount() {
    this.loadedAuthors();
  }

  loadedAuthors = () => {
    const { getAuthorsAction } = this.props;
    getAuthorsAction();
  };

  getAuthorList = () => {
    const {
      getAuthorList: { data, loading, loaded },
    } = this.props;
    const authorList = loaded && data.data.data;
    return { authorList, loaded, loading };
  };

  handleOpenDialog = (values) => () => {
    this.setState({
      isOpenDialog: true,
      data: !values ? null : { ...values },
    });
  };

  handleCloseDialog = () => {
    this.setState({
      isOpenDialog: false,
    });
  };

  handleAddAuthor = (values) => {
    const { addAuthorAction } = this.props;
    addAuthorAction(values).then((res) => this.result(res));
  };

  handleUpdateAuthor = (values) => {
    const { data } = this.state;
    const { updateAuthorAction } = this.props;
    const params = { authorId: data._id };
    if (data._id) updateAuthorAction(params, values).then((res) => this.result(res));
  };

  result = (res) => {
    if (!res.error) {
      this.handleCloseDialog();
      this.loadedAuthors();
    }
  };

  handleRemoveAuthor = (authorId) => () => {
    const { removeAuthorAction } = this.props;
    const params = { authorId };
    removeAuthorAction(params).then((res) => res && this.loadedAuthors());
  };

  render() {
    const { isOpenDialog, data } = this.state;
    const { authorList, loaded, loading } = this.getAuthorList();

    return (
      <section>
        <div className={cx(css.listGroup, css.listGroup_inline)}>
          <h3 className={css.listGroup__title}>Author list</h3>
          <Button variant="contained" size="small" onClick={this.handleOpenDialog()}>
            Add author
          </Button>
        </div>

        <Fetching isFetching={loading}>
          {loaded && authorList.length > 0 && (
            <TableAuthorList
              authorList={authorList}
              handleOpenDialog={this.handleOpenDialog}
              handleRemoveAuthor={this.handleRemoveAuthor}
            />
          )}

          {loaded && authorList.length === 0 && <Well />}
        </Fetching>

        <Dialog isOpenDialog={isOpenDialog} handleCloseDialog={this.handleCloseDialog}>
          <FormAuthorDialog
            data={data}
            handleCloseDialog={this.handleCloseDialog}
            handleSubmitDialog={!data ? this.handleAddAuthor : this.handleUpdateAuthor}
          />
        </Dialog>
      </section>
    );
  }
}

export default connect(
  (state) => ({
    getAuthorList: state.get.getAuthors,
  }),
  (dispatch) => ({
    getAuthorsAction: () => dispatch(getAuthors()),
    addAuthorAction: (data) => dispatch(addAuthor(data)),
    updateAuthorAction: (params, data) => dispatch(updateAuthor(params, data)),
    removeAuthorAction: (params) => dispatch(removeAuthor(params)),
  }),
)(HomeContainer);
