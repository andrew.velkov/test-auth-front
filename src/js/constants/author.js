export const GET_AUTHORS_REQUEST = 'author/getAuthors/REQUEST';
export const GET_AUTHORS_SUCCESS = 'author/getAuthors/SUCCESS';
export const GET_AUTHORS_FAILURE = 'author/getAuthors/FAILURE';

export const ADD_AUTHOR_REQUEST = 'author/addAuthor/REQUEST';
export const ADD_AUTHOR_SUCCESS = 'author/addAuthor/SUCCESS';
export const ADD_AUTHOR_FAILURE = 'author/addAuthor/FAILURE';

export const UPDATE_AUTHOR_REQUEST = 'author/updateAuthor/REQUEST';
export const UPDATE_AUTHOR_SUCCESS = 'author/updateAuthor/SUCCESS';
export const UPDATE_AUTHOR_FAILURE = 'author/updateAuthor/FAILURE';

export const REMOVE_AUTHOR_REQUEST = 'author/removeAuthor/REQUEST';
export const REMOVE_AUTHOR_SUCCESS = 'author/removeAuthor/SUCCESS';
export const REMOVE_AUTHOR_FAILURE = 'author/removeAuthor/FAILURE';
