export const IS_LOGGED_IN_SUCCESS = 'authentication/authentication/SUCCESS';

export const REGISTER_REQUEST = 'user/register/REQUEST';
export const REGISTER_SUCCESS = 'user/register/SUCCESS';
export const REGISTER_FAILURE = 'user/register/FAILURE';

export const LOGING_REQUEST = 'user/login/REQUEST';
export const LOGING_SUCCESS = 'user/login/SUCCESS';
export const LOGING_FAILURE = 'user/login/FAILURE';

export const LOGOUT_REQUEST = 'user/logout/REQUEST';
export const LOGOUT_SUCCESS = 'user/logout/SUCCESS';
export const LOGOUT_FAILURE = 'user/logout/FAILURE';

export const GET_PROFILE_REQUEST = 'user/profile/REQUEST';
export const GET_PROFILE_SUCCESS = 'user/profile/SUCCESS';
export const GET_PROFILE_FAILURE = 'user/profile/FAILURE';
