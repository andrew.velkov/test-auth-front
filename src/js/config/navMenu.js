const NAV_MENU = [
  {
    name: 'Home',
    to: '/home',
    icon: 'home',
  },
];

export default NAV_MENU;
