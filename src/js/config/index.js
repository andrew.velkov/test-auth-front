import API from './api';
import NAV_MENU from './navMenu';
import LANGUAGE from './language';
import HEAD_AUTHOR from './headAuthor';

export { API, NAV_MENU, LANGUAGE, HEAD_AUTHOR };
