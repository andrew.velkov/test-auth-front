## test auth front

### `git clone git@gitlab.com:andrew.velkov/test-auth-front.git`

### `cd test-auth-front`

### `npm install`

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
to view it in the browser.
Open App [http://localhost:3001](http://localhost:3001)

---

Development Build:
### `npm run build:dev`

---

Production Build:
### `npm run build`
